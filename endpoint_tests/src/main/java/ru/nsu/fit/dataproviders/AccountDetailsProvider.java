package ru.nsu.fit.dataproviders;


import org.testng.annotations.DataProvider;

import java.util.UUID;

public class AccountDetailsProvider
{
    private static final String ADMIN_ROLE = "ADMIN";
    private static final String CUSTOMER_ROLE = "CUSTOMER";
    private static final String USER_ROLE = "USER";
    private static final String OWN_LOGIN = "jsmith@nene.nu";
    private static final String OTHER_LOGIN = "iivanov";
    private static final String NONEXISTENT_LOGIN = "divanov";
    private static final UUID OWN_UUID = UUID.fromString("e0e4a9db-54fc-4ac8-b1c5-dfd603ce1b46");
    private static final UUID OTHER_UUID = UUID.fromString("406a6ef6-8751-4003-99ec-820feb6facd5");
    private static final UUID NONEXISTENT_UUID = UUID.fromString("afbb5b2b-91ff-4150-99ec-820feb6facd5");
    private static final String OWN_INFO = "{\r\n" +
            "  \"firstName\" : \"John\",\n" +
            "  \"lastName\" : \"Smith\",\n" +
            "  \"login\" : \"jsmith@nene.nu\",\n" +
            "  \"pswd\" : \"Test@123\",\n" +
            "  \"money\" : 100\n" +
            "}";


    @DataProvider(name = "getCustomerIdProvider")
    public static Object[][] getCustomerIdData()
    {
        return new Object[][]
                {
                        {CUSTOMER_ROLE, OWN_LOGIN, OWN_UUID, 200},
                        {CUSTOMER_ROLE, OTHER_LOGIN, null, 400},
                        {CUSTOMER_ROLE, NONEXISTENT_LOGIN, null, 400},
                        {ADMIN_ROLE, OWN_LOGIN, null, 401},
                        {USER_ROLE, OWN_LOGIN, null, 401},
                };
    }

    @DataProvider(name = "getCustomerInfoProvider")
    public static Object[][] updateBalanceData()
    {
        return new Object[][]
                {
                        {CUSTOMER_ROLE, OWN_UUID, OWN_INFO, 200},
                        {CUSTOMER_ROLE, OTHER_UUID, null, 400},
                        {CUSTOMER_ROLE, NONEXISTENT_UUID, null, 400},
                        {ADMIN_ROLE, OWN_UUID, null, 401},
                        {USER_ROLE, OWN_UUID, null, 401},
                };
    }
}
