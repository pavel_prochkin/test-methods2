package ru.nsu.fit.dataproviders;


import org.testng.annotations.DataProvider;

import java.util.UUID;

public class UserProvider
{
    private static final String ADMIN_ROLE = "ADMIN";
    private static final String CUSTOMER_ROLE = "CUSTOMER";
    private static final String USER_ROLE = "USER";
    private static final String CREATE_GOOD = "{\n\t\"firstName\":\"Michael\",\n\t" +
            "\"lastName\":\"Stronk\",\n\t" +
            "\"login\":\"halloamichael\",\n\t" +
            "\"password\":\"password123\",\n\t" +
            "\"userRole\":\"USER\"\n}";
    private static final String CREATE_BAD = "bad";
    private static final UUID USER_ID_EXISTS = UUID.fromString("c258e188-c7b3-4ecd-b098-65d96d87141e");
    private static final UUID USER_ID_MISSING = UUID.fromString("afbb5b2b-91ff-4150-aea8-1bd447106e30");
    private static final UUID USER_ID_SELF = UUID.fromString("c25659f7-e337-4aa0-bdc5-d2b952ac831b");


    @DataProvider(name = "createUserProvider")
    public static Object[][] createUserData()
    {
        return new Object[][]
                {
                        {CUSTOMER_ROLE, CREATE_GOOD, 200},
                        {CUSTOMER_ROLE, CREATE_BAD, 400},
                        {ADMIN_ROLE, CREATE_GOOD, 401},
                        {USER_ROLE, CREATE_GOOD, 401},
                };
    }


    @DataProvider(name = "changeUserRolesProvider")
    public static Object[][] changeUserRolesData()
    {
        return new Object[][]
                {
                        {CUSTOMER_ROLE, USER_ID_EXISTS, 200},
                        {CUSTOMER_ROLE, USER_ID_MISSING, 400},
                        {ADMIN_ROLE, USER_ID_EXISTS, 401},
                        {USER_ROLE, USER_ID_EXISTS, 401},
                        {USER_ROLE, USER_ID_SELF, 401},
                };
    }
}
