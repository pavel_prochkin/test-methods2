package ru.nsu.fit.dataproviders;

import org.testng.annotations.DataProvider;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;


public class CustomerProvider
{
    private static final String ADMIN_ROLE = "ADMIN";
    private static final String CUSTOMER_ROLE = "CUSTOMER";
    private static final String USER_ROLE = "USER";
    private static final String CREATE_VALID_INPUT = "{\n\t\"firstName\":\"Johnds\",\n\t" +
        "\"lastName\":\"Weakasd\",\n\t" +
        "\"login\":\"hell12@log.com\",\n\t" +
        "\"pswd\":\"password123\",\n\t" +
        "\"money\":\"100\"\n}";
    private static final String CREATE_INVALID_INPUT = "";
    private static final UUID BALANCE_OWN_UUID = UUID.fromString("e0e4a9db-54fc-4ac8-b1c5-dfd603ce1b46");
    private static final UUID BALANCE_OTHER_UUID = UUID.fromString("406a6ef6-8751-4003-99ec-820feb6facd5");
    private static final int BALANCE_GOOD = 1;
    private static final int BALANCE_BAD = -1;
    private static final int BALANCE_BORDER = 0;

    @DataProvider(name = "createCustomerProvider")
    public static Object[][] createCustomerData()
    {
        return new Object[][]
        {
            {ADMIN_ROLE, CREATE_VALID_INPUT, 200},
            {ADMIN_ROLE, CREATE_INVALID_INPUT, 400},
            {CUSTOMER_ROLE, CREATE_VALID_INPUT, 401},
            {USER_ROLE, CREATE_VALID_INPUT, 401}
        };
    }

    @DataProvider(name = "updateBalanceProvider")
    public static Object[][] updateBalanceData()
    {
        return new Object[][]
        {
                {CUSTOMER_ROLE, BALANCE_OWN_UUID, BALANCE_GOOD, 200},
                {CUSTOMER_ROLE, BALANCE_OWN_UUID, BALANCE_BORDER, 200},
                {CUSTOMER_ROLE, BALANCE_OWN_UUID, BALANCE_BAD, 400},
                {CUSTOMER_ROLE, BALANCE_OTHER_UUID, BALANCE_GOOD, 400},
                {CUSTOMER_ROLE, BALANCE_OTHER_UUID, BALANCE_BORDER, 400},
                {CUSTOMER_ROLE, BALANCE_OTHER_UUID, BALANCE_BAD, 400},
                {ADMIN_ROLE, BALANCE_OWN_UUID, BALANCE_GOOD, 401},
                {USER_ROLE, BALANCE_OWN_UUID, BALANCE_GOOD, 401},
        };
    }
}
