package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.dataproviders.CustomerProvider;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.testutils.DataGenerator;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

public class UpdateBalanceTests extends Assert
{
    @Test(dataProvider = "updateBalanceProvider",
            dataProviderClass = CustomerProvider.class)
    @Title("Update balance")
    @Description("Update customer's balance via REST API")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer management")
    public void addFunds(String role, UUID customerId, Integer updateValue, int expected)
    {
        Client client = DataGenerator.getClientForRole(role);

        WebTarget webTarget = client
                .target("http://localhost:8080/endpoint/rest")
                .path("update_balance")
                .path(customerId.toString())
                .path(updateValue.toString());

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(updateValue, MediaType.APPLICATION_JSON));

        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));

        assertEquals(response.getStatus(), expected);
    }
}
