package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.dataproviders.AccountDetailsProvider;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.testutils.DataGenerator;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

public class GetCustomerIdTests extends Assert
{
    @Test(dataProvider = "getCustomerIdProvider",
            dataProviderClass = AccountDetailsProvider.class)
    @Title("Get customer id")
    @Description("Retrieve existing customer's account details via REST API")
    @Severity(SeverityLevel.NORMAL)
    @Features("Account details retrieval")
    public void getCustomerId(String role, String login, UUID expectedId, int expectedCode)
    {
        Client client = DataGenerator.getClientForRole(role);
        WebTarget webTarget = client
                .target("http://localhost:8080/endpoint/rest")
                .path("get_customer_id")
                .path(login);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        String body = response.readEntity(String.class);

        AllureUtils.saveTextLog("Response: " + body);

        assertEquals(response.getStatus(), expectedCode);

        if(expectedCode == 200)
        {
            String expectedStr = "{\"id\":\"" + expectedId.toString() + "\"}";
            assertEquals(body, expectedStr);
        }
    }
}
