package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.dataproviders.UserProvider;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.testutils.DataGenerator;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class CreateUserTests extends Assert
{
    @Test(dataProvider = "createUserProvider",
            dataProviderClass = UserProvider.class)
    @Title("Create user")
    @Description("Create user via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("User creation")
    public void createUser(String role, String newUser, int expected)
    {
        Client client = DataGenerator.getClientForRole(role);
        WebTarget webTarget = client
                .target("http://localhost:8080/endpoint/rest")
                .path("create_user");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(newUser, MediaType.APPLICATION_JSON));

        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));

        assertEquals(response.getStatus(), expected);
    }

}
