package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.dataproviders.UserProvider;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.testutils.DataGenerator;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

public class ChangeUserRoleTests extends Assert
{
    @Test(dataProvider = "changeUserRolesProvider",
            dataProviderClass = UserProvider.class)
    @Title("Change user roles")
    @Description("Change user's roles via REST API")
    @Severity(SeverityLevel.NORMAL)
    @Features("User management")
    public void changeUserRoles(String role, UUID userId, int expected)
    {
        Client client = DataGenerator.getClientForRole(role);

        WebTarget webTarget = client
                .target("http://localhost:8080/endpoint/rest")
                .path("change_user_role")
                .path(userId.toString())
                .path("USER");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.put(Entity.entity("", MediaType.APPLICATION_JSON));

        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));

        assertEquals(response.getStatus(), expected);
    }
}
