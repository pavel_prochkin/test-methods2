package ru.nsu.fit.tests;

import org.junit.AfterClass;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.dataproviders.CustomerProvider;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.testutils.DataGenerator;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class CreateCustomerTests extends Assert
{
    @Test(dataProvider = "createCustomerProvider",
            dataProviderClass = CustomerProvider.class)
    @Title("Create customer")
    @Description("Create customer via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer creation")
    public void createCustomer(String role, String newCustomer, int expected)
    {
        Client client = DataGenerator.getClientForRole(role);
        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("create_customer");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(newCustomer, MediaType.APPLICATION_JSON));

        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));

        assertEquals(response.getStatus(), expected);
    }
}
