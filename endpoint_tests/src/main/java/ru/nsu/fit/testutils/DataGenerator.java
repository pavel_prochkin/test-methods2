package ru.nsu.fit.testutils;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

public class DataGenerator
{
    public static final String ADMIN_ROLENAME = "ADMIN";
    public static final String ADMIN_LOGIN = "admin";
    public static final String ADMIN_PASS = "setup";
    public static final String CUSTOMER_ROLENAME = "CUSTOMER";
    public static final String CUSTOMER_LOGIN = "jsmith@nene.nu";
    public static final String CUSTOMER_PASS = "Test@123";
    public static final String USER_ROLENAME = "USER";
    public static final String USER_LOGIN = "jbinx";
    public static final String USER_PASS = "jajap@ss!";

    public static Client getClientForRole(String roleName)
    {
        String login;
        String pswd;

        switch(roleName)
        {
            case ADMIN_ROLENAME:
            {
                login = ADMIN_LOGIN;
                pswd = ADMIN_PASS;
                break;
            }

            case CUSTOMER_ROLENAME:
            {
                login = CUSTOMER_LOGIN;
                pswd = CUSTOMER_PASS;
                break;
            }

            case USER_ROLENAME:
            {
                login = USER_LOGIN;
                pswd = USER_PASS;
                break;
            }

            default:
            {
                login = "";
                pswd = "";
            }
        }

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(login, pswd);
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.register(feature);
        clientConfig.register(JacksonFeature.class);
        return ClientBuilder.newClient( clientConfig );
    }
}
