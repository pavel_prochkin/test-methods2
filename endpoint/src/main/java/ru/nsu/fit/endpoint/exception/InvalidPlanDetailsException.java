package ru.nsu.fit.endpoint.exception;

/**
 * Created by Eugene on 24.09.2016.
 */
public class InvalidPlanDetailsException extends PlanException {
    public static final String EX_DETAILS_TOO_LONG = "Details parameter is too long";
    public static final String EX_DETAILS_TOO_SHORT = "Details parameter is too short";
    public static final String EX_DETAILS_IS_NULL = "Details parameter is null";

    public InvalidPlanDetailsException() {
        super();
    }

    public InvalidPlanDetailsException(String msg) {
        super(msg);
    }

}
