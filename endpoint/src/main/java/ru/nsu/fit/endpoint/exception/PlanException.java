package ru.nsu.fit.endpoint.exception;

/**
 * Created by Eugene on 24.09.2016.
 */
public class PlanException extends IllegalArgumentException {

    public PlanException() {
        super();
    }

    public PlanException(String msg) {
        super(msg);
    }

}
